FROM ubuntu

RUN apt-get update && apt-get install curl gnupg-agent ffmpeg --assume-yes --no-install-recommends
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs
RUN apt-get install -y npm
RUN apt remove curl -y

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .

EXPOSE 80

CMD [ "npm", "start" ]