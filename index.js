const stream = require("youtube-audio-stream");
var express = require("express");
var path = require("path");
var search = require("youtube-search");
var fs = require("fs");
var favicon = require("serve-favicon");
var cron = require("node-cron");
var rimraf = require("rimraf");

var Base64 = {
  _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
  encode: function(e) {
    var t = "";
    var n, r, i, s, o, u, a;
    var f = 0;
    e = Base64._utf8_encode(e);
    while (f < e.length) {
      n = e.charCodeAt(f++);
      r = e.charCodeAt(f++);
      i = e.charCodeAt(f++);
      s = n >> 2;
      o = ((n & 3) << 4) | (r >> 4);
      u = ((r & 15) << 2) | (i >> 6);
      a = i & 63;
      if (isNaN(r)) {
        u = a = 64;
      } else if (isNaN(i)) {
        a = 64;
      }
      t =
        t +
        this._keyStr.charAt(s) +
        this._keyStr.charAt(o) +
        this._keyStr.charAt(u) +
        this._keyStr.charAt(a);
    }
    return t;
  },
  decode: function(e) {
    var t = "";
    var n, r, i;
    var s, o, u, a;
    var f = 0;
    e = e.replace(/[^A-Za-z0-9+/=]/g, "");
    while (f < e.length) {
      s = this._keyStr.indexOf(e.charAt(f++));
      o = this._keyStr.indexOf(e.charAt(f++));
      u = this._keyStr.indexOf(e.charAt(f++));
      a = this._keyStr.indexOf(e.charAt(f++));
      n = (s << 2) | (o >> 4);
      r = ((o & 15) << 4) | (u >> 2);
      i = ((u & 3) << 6) | a;
      t = t + String.fromCharCode(n);
      if (u != 64) {
        t = t + String.fromCharCode(r);
      }
      if (a != 64) {
        t = t + String.fromCharCode(i);
      }
    }
    t = Base64._utf8_decode(t);
    return t;
  },
  _utf8_encode: function(e) {
    e = e.replace(/rn/g, "n");
    var t = "";
    for (var n = 0; n < e.length; n++) {
      var r = e.charCodeAt(n);
      if (r < 128) {
        t += String.fromCharCode(r);
      } else if (r > 127 && r < 2048) {
        t += String.fromCharCode((r >> 6) | 192);
        t += String.fromCharCode((r & 63) | 128);
      } else {
        t += String.fromCharCode((r >> 12) | 224);
        t += String.fromCharCode(((r >> 6) & 63) | 128);
        t += String.fromCharCode((r & 63) | 128);
      }
    }
    return t;
  },
  _utf8_decode: function(e) {
    var t = "";
    var n = 0;
    var r = (c1 = c2 = 0);
    while (n < e.length) {
      r = e.charCodeAt(n);
      if (r < 128) {
        t += String.fromCharCode(r);
        n++;
      } else if (r > 191 && r < 224) {
        c2 = e.charCodeAt(n + 1);
        t += String.fromCharCode(((r & 31) << 6) | (c2 & 63));
        n += 2;
      } else {
        c2 = e.charCodeAt(n + 1);
        c3 = e.charCodeAt(n + 2);
        t += String.fromCharCode(
          ((r & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63)
        );
        n += 3;
      }
    }
    return t;
  }
};

var app = express();
app.use(favicon(__dirname + "/favicon.ico"));
app.use("/static", express.static(__dirname));
app.use(
  "/fonts/",
  express.static(path.join(__dirname, "/node_modules/bootstrap/dist/fonts"))
);
app.use("/js", express.static(__dirname + "/node_modules/bootstrap/dist/js")); // redirect bootstrap JS
app.use("/js", express.static(__dirname + "/node_modules/jquery/dist")); // redirect JS jQuery
app.use("/js", express.static(__dirname + "/node_modules/amplitudejs/dist")); // redirect AmplitudeJS
app.use("/js", express.static(__dirname + "/node_modules/stoor/dist")); // redirect Stoor
app.use("/js", express.static(__dirname + "/scripts")); // redirect site JS
app.use("/css", express.static(__dirname + "/node_modules/bootstrap/dist/css")); // redirect CSS bootstrap

cron.schedule("0 0 * * *", function() {
  rimraf("/downloads", function() {
    console.log("downloads folder removed");
  });
});

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname + "/templates/index.html"));
});

app.get("/search/:query", function(req, res) {
  var opts = {
    maxResults: 25,
    key: "AIzaSyBA6S3stNrRZX58XD0TXHJ_K12QkS5CcIo",
    type: "video"
  };

  search(req.params.query, opts, function(err, results) {
    if (err) {
      res.sendFile(path.join(__dirname + "/templates/error.html"));
    } else {
      res.setHeader("Content-Type", "application/json");
      res.send(JSON.stringify(results));
    }
  });
});

app.get("/stream/:videoId", function(req, res) {
  stream("youtube.com/watch?v=" + req.params.videoId).pipe(res);
});

app.get("/convert/:videoId", function(req, res) {
  if (!fs.existsSync("downloads")) {
    fs.mkdirSync("downloads");
  }

  var myFile = fs.createWriteStream("downloads/" + req.params.videoId + ".mp3");
  var writeStream = stream("youtube.com/watch?v=" + req.params.videoId).pipe(
    myFile
  );

  writeStream.on("finish", function() {
    var file = __dirname + "/downloads/" + req.params.videoId + ".mp3";
    res.send("finished");
  });
});

app.get("/download/:videoId/:title", function(req, res) {
  var file = __dirname + "/downloads/" + req.params.videoId + ".mp3";
  res.download(file, Base64.decode(req.params.title) + ".mp3");
});

app.listen(80, "0.0.0.0", function() {
  console.log("PlaYT is started on localhost:80");
});
