$(function() {

    var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

  var recent = new Stoor({
    namespace: 'recent'
  })
  var recentIds = recent.get("recentIds");
  var recentList = recent.get("recentList");
  if (recentList == null) {
    recentList = [];
    $('.recent-list').append('<h5>No recent items</h5>')
  }
  if (recentIds == null) recentIds = [];

  function openPlayer(element) {
    Amplitude.pause();
    $('.player-container').empty();
    $('.active-result-item').removeClass("active-result-item");
    $('<a class="amplitude-play-pause" amplitude-main-play-pause="true"><i title="Play audio" class="play-pause-button glyphicon glyphicon-pause" aria-hidden="true"></i></a><a class="amplitude-volume-down"><i class="volume-down-button glyphicon glyphicon-volume-down"></i></a><a class="amplitude-volume-up"><i class="volume-up-button glyphicon glyphicon-volume-up"></i></a><a class="amplitude-stop"><i class="stop-button glyphicon glyphicon-stop"></i></a>')
      .hide().appendTo($(element).parent().children().last()).css('opacity', 0).slideDown(600)
        .animate({
            opacity: 1
          },
          {
            queue: false,
            duration: 1500
          }
        );

    var videoId = $(element).parent().attr('id');

    Amplitude.init({
      "songs": [{
        "url": "/stream/" + videoId
      }],
      "autoplay": true,
      "debug": true,
      "volume": 100
    });

    $('.amplitude-play-pause').on("click", function() {
      $('.amplitude-play-pause i').toggleClass("glyphicon-play");
      $('.amplitude-play-pause i').toggleClass("glyphicon-pause");
    });

    $('.amplitude-stop').on("click", function() {
      $('.amplitude-play-pause i').addClass("glyphicon-play");
      $('.amplitude-play-pause i').removeClass("glyphicon-pause");
    });

    $($(element).parent()).addClass("active-result-item");

    var itemExistsInRecent = (recentIds.indexOf(videoId) > -1);
    if (!itemExistsInRecent) {
      if (recentList.length > 10) {
        recentList.pop();
        recentIds.pop();
      }
      recentList.unshift($(element).parent()[0].outerHTML);
      recentIds.unshift(videoId);
      recent.set("recentList", recentList);
      recent.set("recentIds", recentIds);
    } else {
      var index = recentIds.indexOf(videoId);
      recentIds.splice(index, 1);
      recentList.splice(index, 1);
      recentList.unshift($(element).parent()[0].outerHTML);
      recentIds.unshift(videoId);
      recent.set("recentList", recentList);
      recent.set("recentIds", recentIds);
    }
  };

  function downloadURI(uri) {
    var link = document.createElement("a");
    link.href = uri;
    link.click();
  }

  function convertAndDownload(element){
    var videoId = $(element).parent().parent().parent().attr("id");
    var title = Base64.encode($(element).parent().parent().parent().attr("title"));

    $('.loading-box').css("display", "block");
    $('body :not(.unblurred), body :not(.unblurred) *').addClass("blurr");

    $.get('/convert/' + videoId, function(res) {
      downloadURI('/download/' + videoId + '/' + title);
      $('.loading-box').css("display", "none");
      $('body:not(.unblurred), body:not(.unblurred) *').removeClass("blurr");
    })
  }

  $('#clear-cookies').click(function(){
    recent.clear();
    location.reload();
  });

  $.each(recentList, function(index, recentItem) {
    $('.recent-list').append(recentItem);
    $('.player-container').empty();
    $('.active-result-item').removeClass("active-result-item");
  });

  $('.media .media-body, .media .media-left').on("click", function() {
    openPlayer($(this));
  });

  $('.download-button').on("click", function() {
    convertAndDownload($(this));
  });

  function searchyt() {
    var query = $('#search-input').val();

    $('#search-input').val("");
    $('.result-list').html("");
    $.get("/search/" + query, function(search_results) {
      $.each(search_results, function(index, result) {
        $('.result-list').append(' <div id="' + result.id + '" title="' + result.title + '" class="media"> <div class="media-left"> <img class="d-flex mr-3" src="' + result.thumbnails.high.url +
          '" alt="video thumbnail" /> </div> <div class="media-body"> <h3 class="mt-0">' + result.title + '</h3><div class="result-description">  ' + result.description +
          ' </div></div> <div class="media-right"> <a> <i title="Download mp3" class="download-button glyphicon glyphicon-download" aria-hidden="true"></i> </a> </div> <div class="player-container"></div> </div>');
      });
      $('.recent-list').empty();
      $('.media .media-body, .media .media-left').on("click", function() {
        openPlayer($(this));
      });
      $('.download-button').on("click", function() {
        convertAndDownload($(this));
      });
    });
  };

  $(document).keypress(function(e) {
    if (e.which == 13) {
      searchyt();
    }
  });

  $('#search-button').click(function() {
    searchyt();
  });
});
